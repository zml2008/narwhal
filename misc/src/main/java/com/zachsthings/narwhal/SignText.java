package com.zachsthings.narwhal;

import com.sk89q.commandbook.CommandBook;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.zachsthings.libcomponents.ComponentInformation;
import com.zachsthings.libcomponents.bukkit.BukkitComponent;
import com.zachsthings.libcomponents.config.ConfigurationBase;
import com.zachsthings.libcomponents.config.Setting;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.ChatPaginator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zml2008
 */
@ComponentInformation(friendlyName = "SignText", desc = "Provides right-clickable signs")
public class SignText extends BukkitComponent implements Listener {
    public static final String RAW_NOTICE_HEADER = "[NOTICE]";
    public static final String NOTICE_HEADER = ChatColor.AQUA + RAW_NOTICE_HEADER;
    private LocalConfiguration config;
    @Override
    public void enable() {
        CommandBook.registerEvents(this);
        config = configure(new LocalConfiguration());
        registerCommands(Commands.class);
    }

    @Override
    public void reload() {
        configure(config);
    }

    private static class LocalConfiguration extends ConfigurationBase {
        @Setting("signs") private Map<String, SignMessage> registeredMessages = createDefaultMessage();

        public SignMessage getMessage(String name) {
            return registeredMessages.get(name);
        }

        public Map<String, SignMessage> getRegisteredMessages() {
            return Collections.unmodifiableMap(registeredMessages);
        }

        private static Map<String, SignMessage> createDefaultMessage() {
            Map<String, SignMessage> ret = new HashMap<String, SignMessage>();
            SignMessage firstMessage = new SignMessage();
            firstMessage.text = "&2SignText&r is a plugin that allows easy attachment of text to signs.\n" +
            "Configuration is in the &bplugins/CommandBook/config/signtext.yml&r file";
            ret.put("help", firstMessage);
            return ret;
        }
    }

    private static class SignMessage extends ConfigurationBase {
        @Setting("restricted") private boolean restricted;
        @Setting("text") private String text;

        public boolean isRestricted() {
            return restricted;
        }

        public String getText() {
            return ChatColor.translateAlternateColorCodes('&', text);
        }
    }



    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        if (event.getLine(0).equalsIgnoreCase(RAW_NOTICE_HEADER)) {
            String name = event.getLine(1);
            Player player = event.getPlayer();
            SignMessage msg = config.getMessage(name);
            if (msg == null) {
                player.sendMessage(ChatColor.RED + "This notice does not exist");
                event.setCancelled(true);
                event.getBlock().breakNaturally();
                return;
            }

            boolean canDo = true;
            if (msg.isRestricted()) {
                if (!CommandBook.inst().hasPermission(player, "narwhal.sign.restricted" + name)) {
                    canDo = false;
                }
            } else {
                if (!CommandBook.inst().hasPermission(player, "narwhal.sign.safe." + name)) {
                    canDo = false;
                }
            }
            if (!canDo) {
                event.setCancelled(true);
                event.getBlock().breakNaturally();
                player.sendMessage(ChatColor.RED + "You don't have permission to place this sign");
            } else {
                event.setLine(0, NOTICE_HEADER);
                player.sendMessage(ChatColor.AQUA + "Notice sign successfully created for message " + name);
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        Block block = event.getClickedBlock();
        Material mat = block.getType();
        if (mat == Material.WALL_SIGN || mat == Material.SIGN_POST) {
            Sign sign = (Sign) block.getState();
            if (sign.getLine(0).equalsIgnoreCase(NOTICE_HEADER)) {
                SignMessage msg = config.getMessage(sign.getLine(1));
                if (msg == null) {
                    player.sendMessage(ChatColor.RED + "The message attached to this sign " +
                            "has been removed. Sign will now be disposed of.");
                    block.setType(Material.AIR);
                    return;
                }
                ChatPaginator.ChatPage page = ChatPaginator.paginate(msg.getText(), 1,
                        ChatPaginator.AVERAGE_CHAT_PAGE_WIDTH, ChatPaginator.UNBOUNDED_PAGE_HEIGHT);
                event.getPlayer().sendMessage(page.getLines());
            }
        }
    }

    public class Commands {
        @Command(aliases = "signnotices", desc = "List the available sign notices", max = 1)
        @CommandPermissions("narwhal.sign.list")
        public void listNotices(CommandContext args, CommandSender sender) throws CommandException {
            StringBuilder build = new StringBuilder();
            for (String notice : config.getRegisteredMessages().keySet()) {
                if (build.length() > 0) {
                    build.append(", ");
                }
                build.append(ChatColor.AQUA).append(notice).append(ChatColor.AQUA);
            }
            sender.sendMessage(ChatColor.DARK_AQUA + "Currently available signs: " + build.toString());
        }
    }
}

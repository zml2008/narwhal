package com.zachsthings.narwhal;

import com.sk89q.commandbook.CommandBook;
import com.sk89q.commandbook.session.PersistentSession;
import com.sk89q.commandbook.session.SessionComponent;
import com.sk89q.commandbook.util.PlayerUtil;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.zachsthings.libcomponents.ComponentInformation;
import com.zachsthings.libcomponents.InjectComponent;
import com.zachsthings.libcomponents.bukkit.BukkitComponent;
import com.zachsthings.libcomponents.config.Setting;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.util.Vector;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * @author zml2008
 */
@ComponentInformation(friendlyName = "PermsFlight", desc = "Allows players with the narwhal.fly.onjoin permission to bypass the allow-flight setting in server.properties")
public class PermsFlight extends BukkitComponent implements Listener {
    @InjectComponent private SessionComponent sessions;

    @Override
    public void enable() {
        CommandBook.registerEvents(this);
        registerCommands(Commands.class);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (CommandBook.inst().hasPermission(event.getPlayer(), "narwhal.fly.onjoin")) {
            event.getPlayer().setAllowFlight(true);
        }
    }

    @EventHandler
    public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
        FlightState state = sessions.getSession(FlightState.class, event.getPlayer());
        if (event.getNewValue()) {
            state.applySpeedModifier();
        } else {
            state.applyDefaultSpeedModifier();
        }
    }

    public static class FlightState extends PersistentSession {

        @Setting("speed-modifier") private double speedModifier = 1;
        protected FlightState() {
            super(-1);
        }

        public void setSpeedModifier(double speed) {
            this.speedModifier = speed;
        }

        public void applyDefaultSpeedModifier() {
            applySpeedModifier(1.0, 1.0);
        }

        public void applySpeedModifier() {
            applySpeedModifier(getSpeedModifier(), getGravityModifier());
        }

        private void applySpeedModifier(double air, double gravity) {
            if (hasSpout() && getOwner() instanceof SpoutPlayer) {
                SpoutPlayer player = ((SpoutPlayer) getOwner());
                player.setAirSpeedMultiplier(air);
                //player.setGravityMultiplier(gravity); // TODO: Add proper API to SpoutPlugin and Spoutcraft
            }
        }

        public double getSpeedModifier() {
            return speedModifier;
        }

        public double getGravityModifier() {
            return 1.0 / speedModifier;
        }
    }

    private static boolean hasSpout() {
        try {
            Class.forName("org.getspout.spoutapi.player.SpoutPlayer");
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public class Commands {
        @Command(aliases = "toggleflight", usage = "[player]", desc = "Toggles whether player can fly", max = 1)
        @CommandPermissions("narwhal.fly.toggle")
        public void toggleFlight(CommandContext args, CommandSender sender) throws CommandException {
            Player target = args.argsLength() == 0 ? PlayerUtil.checkPlayer(sender) : PlayerUtil.matchSinglePlayer(sender, args.getString(0));
            if (target != sender) {
                CommandBook.inst().checkPermission(sender, "narwhal.fly.toggle.other");
            }
            target.setAllowFlight(!target.getAllowFlight());
            if (target != sender) {
                sender.sendMessage(ChatColor.BLUE + "Player " + PlayerUtil.toName(sender) + " " + (target.getAllowFlight() ? "can now" : "can no longer") + " fly.");
            }
                target.sendMessage(ChatColor.BLUE + "You " + (target.getAllowFlight() ? "can now" : "can no longer") + " fly.");
        }

        @Command(aliases = "fly", desc = "Enable or disable flight", max = 0)
        @CommandPermissions("narwhal.fly")
        public void fly(CommandContext args, CommandSender sender) throws CommandException {
            final Player ply = PlayerUtil.checkPlayer(sender);
            boolean val = !ply.isFlying();
            if (CommandBook.callEvent(new PlayerToggleFlightEvent(ply, val)).isCancelled()) {
                throw new CommandException("Flight couldn't be toggled because the event was cancelled!");
            }
            if (val) {
                ply.setAllowFlight(true);
                ply.setVelocity(ply.getVelocity().add(new Vector(0, 2, 0)));
            }
            ply.setFlying(val);
            ply.sendMessage(ChatColor.BLUE + "You " + (val ? "are now" : "are no longer") + " flying");
        }

        @Command(aliases = {"speed", "setspeed"}, desc = "Set moving speed", usage = "speed", max = 1)
        public void speed(CommandContext args, CommandSender sender) throws CommandException {
            if (!hasSpout()) {
                throw new CommandException("SpoutPlugin is required to use this command!");
            }
            Player player = PlayerUtil.checkPlayer(sender);
            FlightState session = sessions.getSession(FlightState.class, player);
            session.setSpeedModifier(args.getDouble(0, 1));
            sender.sendMessage(ChatColor.YELLOW + "Your speed multiplier set to " + session.getSpeedModifier());
            if (player.isFlying()) {
                session.applySpeedModifier();
            }
        }
    }
}

package com.zachsthings.narwhal;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author zml2008
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredService {
}

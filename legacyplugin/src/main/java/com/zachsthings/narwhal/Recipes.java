/**
 * 
 */
package com.zachsthings.narwhal;

import org.bukkit.inventory.*;
import org.bukkit.Material;
import java.util.List;
import java.util.ArrayList;

/**
 * @author zml2008
 *
 */
public class Recipes {
    private final Narwhal plugin;
        public Recipes(final Narwhal plugin) {
        this.plugin = plugin;
    }

        public void addCrafting() {
            List<Recipe> crafting = new ArrayList<Recipe>();
            ShapedRecipe web = new ShapedRecipe(new ItemStack(Material.WEB, 2)).shape("###", "###","###").setIngredient('#', Material.STRING);
            crafting.add(web);
            ShapedRecipe rails = new ShapedRecipe(new ItemStack(Material.RAILS, 64)).shape("# #","#/#", "# #").setIngredient('#', Material.IRON_INGOT).setIngredient('/', Material.STICK);
            crafting.add(rails);
            // Chainmail Armor
            ShapedRecipe chainhat = new ShapedRecipe(new ItemStack(Material.CHAINMAIL_HELMET,1)).shape("###", "# #").setIngredient('#', Material.WEB);
            ShapedRecipe chainchest = new ShapedRecipe(new ItemStack(Material.CHAINMAIL_CHESTPLATE,1)).shape("# #", "###", "###").setIngredient('#', Material.WEB);
            ShapedRecipe chainpants = new ShapedRecipe(new ItemStack(Material.CHAINMAIL_LEGGINGS,1)).shape("###", "# #", "# #").setIngredient('#', Material.WEB);
            ShapedRecipe chainshoes = new ShapedRecipe(new ItemStack(Material.CHAINMAIL_BOOTS,1)).shape("# #", "# #").setIngredient('#', Material.WEB);
            crafting.add(chainhat);
            crafting.add(chainchest);
            crafting.add(chainpants);
            crafting.add(chainshoes);
            ShapedRecipe slimeball = new ShapedRecipe(new ItemStack(Material.SLIME_BALL, 1)).shape("---", "-.-", "---").setIngredient('-', Material.LEAVES).setIngredient('.', Material.SNOW_BALL);
            crafting.add(slimeball);
            
            for (Recipe recipe: crafting) {
                plugin.getServer().addRecipe(recipe);
            }
        }
}

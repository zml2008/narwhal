package com.zachsthings.narwhal;

import com.avaje.ebean.validation.NotEmpty;
import com.avaje.ebean.validation.NotNull;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity()
@Table(name="n_nicks")
public class NickPersist {
    @Id
    private int id;
    
    @NotNull
    private String realName;
    
    @NotEmpty
    private String fakeName;
    
    public void setId (int id) {
        this.id = id;
    }
    
    public int getId () {
        return id;
    }
    
    public void setRealName(String name) {
        this.realName = name;
    }
    
    public String getRealName() {
        return realName;
    }
    
    public void setFakeName(String name) {
        this.fakeName = name;
    }
    
    public String getFakeName() {
        return fakeName;
    }
    
}

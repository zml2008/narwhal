package com.zachsthings.narwhal.gui;

import com.zachsthings.narwhal.Narwhal;
import org.getspout.spoutapi.gui.Color;
import org.getspout.spoutapi.gui.ContainerType;
import org.getspout.spoutapi.gui.GenericContainer;
import org.getspout.spoutapi.gui.GenericGradient;
import org.getspout.spoutapi.gui.GenericLabel;
import org.getspout.spoutapi.gui.RenderPriority;
import org.getspout.spoutapi.gui.WidgetAnchor;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * @author zml2008
 */
public class StatusBar extends GenericContainer {
    private static final Color DARK_GREY = new Color(27, 27, 27);
    
    public StatusBar(SpoutPlayer player) {
        setWidth(player.getMainScreen().getWidth()).setHeight(20);
        setLayout(ContainerType.OVERLAY);
        setX(0).setY(0);
        addChild(new GenericGradient(DARK_GREY).setPriority(RenderPriority.Highest).setAnchor(WidgetAnchor.CENTER_CENTER));
    }

    public void setUp(SpoutPlayer player) {
        addChild(new GenericLabel("Narwhal").setAnchor(WidgetAnchor.BOTTOM_LEFT));
        player.getMainScreen().attachWidget(Narwhal.inst(), this);
    }
}

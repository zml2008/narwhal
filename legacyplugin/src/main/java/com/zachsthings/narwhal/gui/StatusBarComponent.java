package com.zachsthings.narwhal.gui;

import com.zachsthings.narwhal.Narwhal;
import com.zachsthings.narwhal.components.AbstractComponent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * @author zml2008
 */
public class StatusBarComponent extends AbstractComponent implements Listener {
    
    @Override
    public void postInitialize() {
        for (SpoutPlayer player : SpoutManager.getOnlinePlayers()) {
            new StatusBar(player).setUp(player);
        }
        Narwhal.server().getPluginManager().registerEvents(this, Narwhal.inst());
    }
    
    @EventHandler
    public void join(PlayerJoinEvent event) {
        SpoutPlayer player = SpoutManager.getPlayer(event.getPlayer());
		player.setAirSpeedMultiplier(2);
        new StatusBar(player).setUp(player);
    }
}

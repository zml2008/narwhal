package com.zachsthings.narwhal;

    import com.avaje.ebean.validation.NotEmpty;
    import javax.persistence.Entity;
    import javax.persistence.Id;
    import javax.persistence.Table;
    import org.bukkit.entity.Player;
    
@Entity()
@Table(name="n_board")
public class BoardHandler {
    @Id
    private int id;
    
    @NotEmpty
    private String playerName;
    
    @NotEmpty
    private String msg;
    
    public void setId(int id) {
        this.id = id;    
    }
    public int getId() {
        return id;   
    }
    
    public String getPlayer() {
        return playerName;
    }
    
    public void setPlayer(Player player) {
        this.playerName = player.getName();
    }
    
    public String getMsg() {
        return msg;
    }
    
    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    public void addMessage(Player player, String msg) {
        this.playerName = player.getName();
        this.msg = msg;
    }
    
    

}
package com.zachsthings.narwhal;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.util.yaml.YAMLFormat;
import com.sk89q.util.yaml.YAMLNode;
import com.sk89q.util.yaml.YAMLProcessor;
import com.sk89q.wepif.PermissionsResolverManager;
import com.zachsthings.narwhal.components.AbstractComponent;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.command.*;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import com.sk89q.minecraft.util.commands.*;
// Database
import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceException;
// Commands
import com.zachsthings.narwhal.commands.*;

import org.bukkit.ChatColor;

/**
 * Plugin for Bukkit servers zml2008 uses
 *
 * @author zml2008
 * @version WithoutVersioning
 */
public class Narwhal extends JavaPlugin {
    //Integration with other stuff
    protected static final Logger logger = Logger.getLogger("Minecraft.NarwhalPlugin");

    private YAMLProcessor config;

    private static Narwhal instance;

    public static Narwhal inst() {
        return instance;
    }

    public static Server server() {
        return Bukkit.getServer();
    }

    public Narwhal() {
        super();
        instance = this;
    }
    
    /**
     * List of commands.
     */
    protected CommandsManager<CommandSender> commands;

    protected List<AbstractComponent> components = new ArrayList<AbstractComponent>();

    private CommandInjector defaultInjector = new CommandInjector(this);
    
    public void onDisable() {
        for (AbstractComponent component : components) {
            component.unload();
            unregisterCommands(component);
        }
        components.clear();
        config.save();
        PluginDescriptionFile pdfFile = this.getDescription();
        logger.info( pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!" );
    }

    public void onEnable() {
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.getParentFile().exists()) {
            configFile.getParentFile().mkdirs();
        }
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        config = new YAMLProcessor(configFile, true, YAMLFormat.EXTENDED);
        try {
            config.load();
        } catch (IOException e) {
            logger.severe("Error loading configuration: " + e);
            e.printStackTrace();
        }
        commands = new CommandsManager<CommandSender>() {
            @Override
            public boolean hasPermission(CommandSender player, String perm) {
                return Narwhal.this.hasPermission(player, perm);
            }
        };
        PermissionsResolverManager.initialize(this);
        
        // setupPermissions();
        // Database Stuff
        setupDatabase();

        // Register  commands
        commands.setInjector(defaultInjector);
        commands.register(GeneralCommands.class);
        loadComponents();
        enableComponents();

        config.save();
        PluginDescriptionFile pdfFile = this.getDescription();
        logger.info( pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!" );
    }

    /**
     * Setup persistence DB
     */
    private void setupDatabase() {
        try {
            getDatabase().find(NickPersist.class).findRowCount();
        } catch (PersistenceException ex) {
            logger.warning("Installing database for " + this.getDescription().getName() + " due to first time usage");
            this.installDDL();
        }
    }
    @Override
    public List<Class<?>> getDatabaseClasses() {
        List<Class<?>> list = new ArrayList<Class<?>>();
        list.add(NickPersist.class);
        return list;
    }

    public void registerCommands(Class<? extends BaseCommand> clazz, AbstractComponent component)  {
        commands.setInjector(new CommandInjector(component));
        List<Command> registered = commands.registerAndReturn(clazz);
        SimpleCommandMap commandMap = getField(getServer().getPluginManager(), "commandMap");
        if (registered == null || commandMap == null) {
            return;
        }
        for (Command command : registered) {
            commandMap.register(getDescription().getName(), new NarwhalBukkitCommand(commands, command, component));
        }
        commands.setInjector(defaultInjector);
    }

    public void unregisterCommands(AbstractComponent component) {
        SimpleCommandMap commandMap = getField(getServer().getPluginManager(), "commandMap");
        List<String> toRemove = new ArrayList<String>();
        Map<String, org.bukkit.command.Command> knownCommands = getField(SimpleCommandMap.class, commandMap, "knownCommands");
        Set<String> aliases = getField(SimpleCommandMap.class, commandMap, "aliases");
        for (Iterator<org.bukkit.command.Command> i = knownCommands.values().iterator(); i.hasNext();) {
            org.bukkit.command.Command cmd = i.next();
            if (cmd instanceof NarwhalBukkitCommand && ((NarwhalBukkitCommand) cmd).getComponent().equals(component)) {
                i.remove();
                for (String alias : cmd.getAliases()) {
                    org.bukkit.command.Command aliasCmd = knownCommands.get(alias);
                    if (cmd.equals(aliasCmd)) {
                        aliases.remove(alias);
                        toRemove.add(alias);
                    }
                }
            }
        }
        for (String string : toRemove) {
            knownCommands.remove(string);
        }
    }

    private <T> T getField(Object from, String name) {
        try {
            Field field = from.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return (T)field.get(from);
        } catch (NoSuchFieldException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        }
    }

    private <T, V> V getField(Class<T> clazz,  T from, String name) {
        try {
            Field field = clazz.getDeclaredField(name);
            field.setAccessible(true);
            return (V)field.get(from);
        } catch (NoSuchFieldException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        }
    }

    private void loadComponents() {
        List<String> disabledComponents = config.getStringList("components.disabled", null);
        List<String> stagedEnabled = config.getStringList("components.enabled", null);
        for (Iterator<String> i = stagedEnabled.iterator(); i.hasNext(); ) {
            String nextName = i.next();
            Class<?> next = null;
            try {
                next = Class.forName(nextName);
            } catch (ClassNotFoundException e) {}

            if (next == null || !AbstractComponent.class.isAssignableFrom(next)) {
                logger.warning("Narwhal: Invalid or unknown class found in enabled components: "
                        + nextName + ". Moving to disabled components list.");
                i.remove();
                disabledComponents.add(nextName);
                continue;
            }

            try {
                Constructor<? extends AbstractComponent> construct = next.asSubclass(AbstractComponent.class).getConstructor();
                components.add(construct.newInstance());
            } catch (InvocationTargetException e) {
                e.printStackTrace();
                continue;
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

        }

        config.setProperty("components.disabled", disabledComponents);
        config.setProperty("components.enabled", stagedEnabled);
    }

    public void enableComponents() {
        for (AbstractComponent component : components) {
            YAMLNode componentConfig = config.getNode("component." + component.getClass().getSimpleName());
            if (componentConfig == null) {
                componentConfig = config.addNode("component." + component.getClass().getSimpleName());
            }
            component.init(componentConfig);
            for (Field field : component.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(RequiredService.class)) {
                    setFieldService(field, field.getType(), component);
                }
            }
            logger.info("Component " + component.getClass().getSimpleName() + " successfully enabled!");
            component.postInitialize();
        }

    }

    public <T> void setFieldService(Field field, Class<T> service, AbstractComponent component) {
        T registration = getServer().getServicesManager().load(service);
        field.setAccessible(true);
        try {
            field.set(component, registration);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Called on a command.
     */
    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd,
            String commandLabel, String[] args) {
        try {
            commands.execute(cmd.getName(), args, sender, sender);
        } catch (CommandPermissionsException e) {
            sender.sendMessage(ChatColor.RED + "You don't have permission.");
        } catch (MissingNestedCommandException e) {
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (CommandUsageException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (WrappedCommandException e) {
            if (e.getCause() instanceof NumberFormatException) {
                sender.sendMessage(ChatColor.RED + "Number expected, string received instead.");
            } else {
                sender.sendMessage(ChatColor.RED + "An error has occurred. See console.");
                e.printStackTrace();
            }
        } catch (CommandException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
        }
        
        return true;
    }
    
    public boolean hasPermission(CommandSender sender, String perm) {
        if (sender instanceof OfflinePlayer) {
            return sender.isOp() || getPermissionsResolver().hasPermission((OfflinePlayer) sender, perm);
        } else {
            return sender.isOp();
        }
    }
    
    /**
     * Checks permissions and throws an exception if permission is not met.
     * 
     * @param sender
     * @param perm
     * @throws CommandPermissionsException 
     */
    public void checkPermission(CommandSender sender, String perm)
            throws CommandPermissionsException {
        if (!hasPermission(sender, perm)) {
            throw new CommandPermissionsException();
        }
    }

    public PermissionsResolverManager getPermissionsResolver() {
        return PermissionsResolverManager.getInstance();
    }

    public static Logger logger() {
        return logger;
    }
}

 

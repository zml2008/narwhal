// $Id$
/*
 * CommandBook
 * Copyright (C) 2010, 2011 sk89q <http://www.sk89q.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package com.zachsthings.narwhal.commands;

import com.sk89q.minecraft.util.commands.*;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.NestedCommand;
import com.zachsthings.narwhal.Narwhal;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GeneralCommands {
    private final Narwhal plugin;

    public GeneralCommands(Narwhal plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = {"narwhal"}, desc = "Narwhal commands",
            flags = "", min = 1, max = 1)
    @NestedCommand({NarwhalCommands.class})
    public void narwhal() {
    }
    
    @Command(aliases = {"health"},
            desc = "Manage player health",
            flags = "", min = 2, max = 3)
    @NestedCommand({HealthCommands.class})
    public void health() {
    }
    
    @Command(aliases = {"sneak"},
            usage = "", desc = "Toggle serverside sneaking",
            flags = "g", min = 0, max = 0)
    @CommandPermissions({"narwhal.sneak"})
    public void sneak(CommandContext args,
            CommandSender sender) throws CommandException {
        Player player = (Player)sender;
        if (!args.hasFlag('g')) {
            player.setSneaking(!(player.isSneaking()));
            if (player.isSneaking()) {
                player.sendMessage(ChatColor.BLUE + "You are now sneaking!");
            } else {
                player.sendMessage(ChatColor.BLUE + "You are no longer sneaking");
            }
        } else if (player.isSneaking()) { 
            player.sendMessage(ChatColor.BLUE + "You are already sneaking");
        }   
    }
    /* Added in CommandBook
    @Command(aliases = {"faketime"},
            usage = "[day/night]", desc = "Send a fake time to yourself",
            flags = "", min = 0, max = 1)
    @CommandPermissions({"narwhal.faketime"})
    public void faketime(CommandContext args, CommandSender sender) throws CommandException {
        Player player = plugin.checkPlayer(sender);
        player.resetPlayerTime();
        if (args.argsLength() == 1) {
        String time = args.getString(0);
            if (time.equalsIgnoreCase("day")) {
                player.setPlayerTime(4000, false);
                player.sendMessage(ChatColor.YELLOW + "Your time set to day.");
            } else if (time.equalsIgnoreCase("night")) {
                player.setPlayerTime(16000, false);
                player.sendMessage(ChatColor.YELLOW + "Your time set to night.");
            } else {
                throw new CommandException("Unknown time value entered");
            }
        } else {
            player.resetPlayerTime();
            player.sendMessage(ChatColor.YELLOW + "Your time reset to world time!");
        }
    } */
}

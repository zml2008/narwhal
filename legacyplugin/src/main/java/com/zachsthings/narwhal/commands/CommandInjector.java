package com.zachsthings.narwhal.commands;

import com.sk89q.minecraft.util.commands.Injector;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class CommandInjector implements Injector {

    private Object plugin;

    public CommandInjector (Object plugin) {
        this.plugin = plugin;
    }

    public Object getInstance(Class<?> cls) throws InvocationTargetException,
            IllegalAccessException, InstantiationException {
        try {
            Constructor<?> construct = cls.getConstructor(plugin.getClass());
            return construct.newInstance(plugin);
        } catch (NoSuchMethodException e) {
            try {
                Constructor<?> construct = cls.getConstructor();
                return construct.newInstance();
            } catch (NoSuchMethodException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

            return null;
    }
}

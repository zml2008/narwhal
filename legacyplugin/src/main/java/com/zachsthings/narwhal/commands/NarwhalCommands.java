// $Id$
/*
 * CommandBook
 * Copyright (C) 2010, 2011 sk89q <http://www.sk89q.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package com.zachsthings.narwhal.commands;

import com.sk89q.minecraft.util.commands.*;
import com.zachsthings.narwhal.Narwhal;
import org.bukkit.command.CommandSender;

public class NarwhalCommands {
    private final Narwhal plugin;

    public NarwhalCommands(Narwhal plugin) {
        this.plugin = plugin;
    }
    
    @Command(aliases = {"reload"}, desc = "Reload configs", max = 0)
    @CommandPermissions({"narwhal.reload"})
    public void reload(CommandContext args,
            CommandSender sender) throws CommandException {
        plugin.reloadConfig();
        
    }
}

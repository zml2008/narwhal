
package com.zachsthings.narwhal.commands;

import org.bukkit.entity.Player;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.zachsthings.narwhal.Narwhal;

/**
 * Handler for the /health health modification command.
 * @author zml2008
 */
public class HealthCommands extends BaseCommand {
    private Narwhal plugin;

    public HealthCommands(Narwhal plugin) {
        this.plugin = plugin;
    }
    @Command(aliases = {"get"},
            usage = " get [player]",
            desc = "Gets a player's health",
            flags = "", min = 1, max = 1)
    @CommandPermissions ({"narwhal.hearth.get"})
    public void get(CommandContext args,
            CommandSender sender) throws CommandException {
            String name = args.getString(0);
                float health = plugin.getServer().getPlayer(name).getHealth();
                sender.sendMessage(ChatColor.BLUE + name + " has " + health + " hearts.");
            
         }
    @Command(aliases = {"set"},
                usage = "-s [player] <value>",
                desc = "Sets a player's health",
                flags = "s", min = 1, max = 2)
    @CommandPermissions({"narwhal.health.set"})
    public void set(CommandContext args,
                CommandSender sender) throws CommandException {
        if (args.argsLength() == 1) {
            Player player = checkPlayer(sender);
            double hearts = args.getDouble(0)*2;
            player.setHealth((int)hearts);
            sender.sendMessage(ChatColor.BLUE  + "Your health set to " + hearts/2 + " hearts.");
        } else {
            plugin.checkPermission(sender, "narwhal.health.set.other");
            Player player = plugin.getServer().getPlayer(args.getString(0));
            if (player != null) {
                double hearts = args.getDouble(1)*2;
                player.setHealth((int)hearts);
                sender.sendMessage(ChatColor.BLUE  + ChatColor.stripColor(player.getDisplayName()) + "\'s health set to " + hearts/2 + " hearts.");
            }
        }
    }
}

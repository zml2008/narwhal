package com.zachsthings.narwhal.blocks;

import com.zachsthings.narwhal.Narwhal;
import org.bukkit.block.BlockFace;
import org.getspout.spoutapi.block.SpoutBlock;
import org.getspout.spoutapi.material.item.GenericCustomItem;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * @author zml2008
 */
public class BanHammerItem extends GenericCustomItem {
    public BanHammerItem(String texture) {
        super(Narwhal.inst(), "Ban Hammer", texture);
    }

    @Override
    public boolean onItemInteract(SpoutPlayer player, SpoutBlock block, BlockFace face) {

        return true;
    }
}

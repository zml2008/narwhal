package com.zachsthings.narwhal.blocks;

import com.zachsthings.narwhal.Narwhal;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.getspout.spoutapi.material.MaterialData;
import org.getspout.spoutapi.material.block.GenericCustomBlock;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * @author zml2008
 */
public class PyramidBlock extends GenericCustomBlock {

    public PyramidBlock(Plugin plugin, Material type) {
        super(plugin, type.name() + " Pyramid", MaterialData.getBlock(type.getId()).isOpaque());
        setBlockDesign(new PyramidBlockDesign(plugin, "http://files.zachsthings.com/spout/block/PyramidTexture.png", 16));
        setItemDrop(new ItemStack(type, 1));
    }

    public void onNeighborBlockChange(World world, int i, int i1, int i2, int i3) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onBlockPlace(World world, int i, int i1, int i2) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onBlockPlace(World world, int i, int i1, int i2, LivingEntity livingEntity) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onBlockDestroyed(World world, int i, int i1, int i2) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onBlockDestroyed(World world, int i, int i1, int i2, LivingEntity livingEntity) {
    }

    public boolean onBlockInteract(World world, int i, int i1, int i2, SpoutPlayer spoutPlayer) {
        return false;
    }

    public void onEntityMoveAt(World world, int i, int i1, int i2, Entity entity) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void onBlockClicked(World world, int i, int i1, int i2, SpoutPlayer spoutPlayer) {
        //To change body of implemqented methods use File | Settings | File Templates.
    }

    public boolean isProvidingPowerTo(World world, int i, int i1, int i2, BlockFace blockFace) {
        return false;
    }

    public boolean isIndirectlyProvidingPowerTo(World world, int i, int i1, int i2, BlockFace blockFace) {
        return false;
    }

    public static PyramidBlock registerPyramid(Material mat) {
        return new PyramidBlock(Narwhal.inst(), mat);
    }
}

package com.zachsthings.narwhal.blocks;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.zachsthings.narwhal.Narwhal;
import com.zachsthings.narwhal.commands.BaseCommand;
import com.zachsthings.narwhal.components.AbstractComponent;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.inventory.SpoutItemStack;

/**
 * @author zml2008
 */
public class BlockComponent extends AbstractComponent {
    private PyramidBlock block;
    @Override
    public void postInitialize() {
        block = PyramidBlock.registerPyramid(Material.GLOWSTONE);
        Narwhal.inst().registerCommands(BlockCommands.class, this);
    }

    public class BlockCommands extends BaseCommand {
        @Command(aliases = {"givepyramid"},
                desc = "Give a pyramid", usage = "[amt]")
        public void givePyramid(CommandContext args, CommandSender sender) throws CommandException {
            Player player = checkPlayer(sender);
            int amt = args.argsLength() < 1 ? 1 : args.getInteger(0);
            player.getInventory().addItem(new SpoutItemStack(block, amt));
        }
    }
}

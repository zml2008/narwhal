package com.zachsthings.narwhal.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.net.InetAddress;

/**
 * Called on authentication with minecraft.net
 */
public class PlayerAuthenticateEvent extends Event {

    private static final long serialVersionUID = -7821835996257177514L;
    private boolean sessionOfflineMode;
    private InetAddress ipAddress;
    private String name;

    public PlayerAuthenticateEvent(String name, InetAddress ipAddress, boolean sessionOfflineMode) {
        this.name = name;
        this.ipAddress = ipAddress;
        this.sessionOfflineMode = sessionOfflineMode;
    }

    /**
     * @return The player's name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The player's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the IP of the connecting player
     */
    public InetAddress getAddress() {
        return ipAddress;
    }

    public boolean getSessionOfflineMode() {
        return sessionOfflineMode;
    }

    private static final HandlerList handlers = new HandlerList();
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
    
}

package com.zachsthings.narwhal.events;

import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author zml2008
 */
public class BlockPhysicsBreakEvent extends Event implements Cancellable {
    private final Block block;
    private final int changedType;
    private boolean cancelled;

    public BlockPhysicsBreakEvent(Block block, int changedType) {
        this.block = block;
        this.changedType = changedType;
    }
    private static final HandlerList handlers = new HandlerList();
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    public Block getBlock() {
        return block;
    }

    public int getChangedType() {
        return changedType;
    }
}

package com.zachsthings.narwhal.components;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.zachsthings.narwhal.Narwhal;
import com.zachsthings.narwhal.commands.BaseCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author zml2008
 */
public class FunComponent extends AbstractComponent {
    @Override
    public void postInitialize() {
        Narwhal.inst().registerCommands(FunCommands.class, this);
    }

    private static final String[] ripSteveLines = new String[]{"Here's to the crazy ones.",
                "The misfits.",
                "The rebels.",
                "The troublemakers.",
                "The round pegs in the square holes.",
                "Steve Jobs, 1955-2011.",
                "Rest in Peace..."};
    public class FunCommands extends BaseCommand {
		@Command(aliases = {"raw"},
				desc = "Send raw text to a client",
				min = 1, max = -1)
		@CommandPermissions("narwhal.util.raw")
		public void raw(CommandContext args, CommandSender sender) throws CommandException {
			Player target;
			String message;
			if (args.argsLength() == 1) {
				target = checkPlayer(sender);
				message = args.getJoinedStrings(0);
			} else {
				target = Bukkit.getServer().getPlayer(args.getString(0));
				message = args.getJoinedStrings(1);
				if (target == null) throw  new CommandException("Unknown player entered!");
			}
			target.sendRawMessage(message);
		}

		@Command(aliases = {"transmute"},
				desc = "Transmutes a client to a specific entity",
				min = 3, max = 3, flags = "p:")
		@CommandPermissions("narwhal.util.raw")
		public void transmute(CommandContext args, CommandSender sender) throws CommandException {
			Player target;
			if (!args.hasFlag('p')) {
				target = checkPlayer(sender);
			} else {
				target = Bukkit.getServer().getPlayer(args.getString(0));
				if (target == null) throw  new CommandException("Unknown player entered!");
			}
			target.sendRawMessage("\u00A7f\u00a75\u00a7dt" + args.getString(0) + "|" + args.getInteger(1) + "|" + args.getInteger(2));
		}

        @Command(aliases = {"ripsteve"},
                desc = "RIP Steve Jobs", flags = "",
                min = 0, max = 0)
        @CommandPermissions({"commandbook.ripsteve"})
        public void ripSteve(CommandContext args,
                                    final CommandSender sender) {
            int j = 0;
            sender.sendMessage(ChatColor.BLUE + ripSteveLines[0]);
            for (int i = 1; i < ripSteveLines.length; ++i) {
                final int index = i;
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Narwhal.inst(),
                        new Runnable() {
                            public void run() {
                                sender.sendMessage(ChatColor.BLUE + ripSteveLines[index]);
                            }
                        }, j += 10);
            }
        }
    }
}

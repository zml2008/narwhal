package com.zachsthings.narwhal.components;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.zachsthings.narwhal.Narwhal;
import com.zachsthings.narwhal.commands.BaseCommand;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.block.design.GenericCubeBlockDesign;
import org.getspout.spoutapi.inventory.SpoutItemStack;
import org.getspout.spoutapi.material.block.GenericCustomBlock;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * @author zml2008
 */
public class TestingComponent extends AbstractComponent {
    private GenericCustomBlock whiteTest;
    @Override
    public void postInitialize() {
        SpoutManager.getFileManager().addToPreLoginCache(Narwhal.inst(), "http://files.zachsthings.com/spout/block/blankWhite.png");
        whiteTest = new GenericCustomBlock(Narwhal.inst(), "blankWhite", true) {
            public void onNeighborBlockChange(World world, int x, int y, int z, int changedId) {}
            public void onBlockPlace(World world, int x, int y, int z) {}
            public void onBlockPlace(World world, int x, int y, int z, LivingEntity living) {}
            public void onBlockDestroyed(World world, int x, int y, int z) {}

            public void onBlockDestroyed(World world, int i, int i1, int i2, LivingEntity livingEntity) {
            }

            public boolean onBlockInteract(World world, int x, int y, int z, SpoutPlayer player) {return false; }
            public void onEntityMoveAt(World world, int x, int y, int z, Entity entity) {}
            public void onBlockClicked(World world, int x, int y, int z, SpoutPlayer player) {}

            public boolean isProvidingPowerTo(World world, int x, int y, int z, BlockFace face) {
                return false;
            }

            public boolean isIndirectlyProvidingPowerTo(World world, int x, int y, int z, BlockFace face) {
                return false;
            }
        };
        whiteTest.setBlockDesign(new GenericCubeBlockDesign(Narwhal.inst(), "http://files.zachsthings.com/spout/block/blankWhite.png", 16));
        whiteTest.setLightLevel(9);
        whiteTest.setHardness(0.2F);
        Narwhal.inst().registerCommands(TestingCommands.class, this);
    }

    public class TestingCommands extends BaseCommand {
        @Command(aliases = {"spoutgive"},
                usage = " [-p name] <item> [amount]", desc = "Gives you or a specified player a spout item",
                flags = "p:", min = 1, max = 2)
        @CommandPermissions("narwhal.spout.give")
        public void spoutGive(CommandContext args, CommandSender sender) throws CommandException {
            Player player;
            ItemStack itemToGive;
            if (args.hasFlag('p')) {
                player = Bukkit.getServer().getPlayer(args.getFlag('p'));
            } else {
                player = checkPlayer(sender);
            }
            if (player == null) throw new CommandException("Unknown player given!");
            int size = args.argsLength() > 1 ? args.getInteger(1) : 1;
            itemToGive = new SpoutItemStack(whiteTest, size);
            player.getInventory().addItem(itemToGive);
        }
    }
}

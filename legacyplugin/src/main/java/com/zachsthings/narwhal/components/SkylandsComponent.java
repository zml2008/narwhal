package com.zachsthings.narwhal.components;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.zachsthings.narwhal.Narwhal;
import com.zachsthings.narwhal.commands.BaseCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.List;

/**
 * @author zml2008
 */
public class SkylandsComponent extends AbstractComponent implements Listener {

    @Override
    public void postInitialize() {
        Bukkit.getServer().createWorld(WorldCreator.name(
                getSkyWorldName()).
                environment(World.Environment.THE_END));
        Narwhal.server().getPluginManager().registerEvents(this, Narwhal.inst());
        Narwhal.inst().registerCommands(SkylandsCommands.class, this);
    }

    public String getSkyWorldName() {
        return getConfiguration().getString("name", "skylands");
    }

    @EventHandler
    public void voidDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player) || event.getCause() != EntityDamageEvent.DamageCause.VOID ) {
            return;
        }
        Player player = (Player)event.getEntity();
        directToggle(getSkyWorldName(), player);

    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Location loc = event.getTo();
        if (loc.getY() > 127) {
            event.setCancelled(true);
            directToggle(getSkyWorldName(), player);
        }
    }


    public void skyToggle(String name, Player player) {
        List<World> worlds = Bukkit.getServer().getWorlds();
        World norm = null;
        for (World world: worlds) {
            if (world.getEnvironment() == World.Environment.NORMAL) {
                norm = world;
                break;
            }
        }
        
        World.Environment enviro = player.getWorld().getEnvironment();
        if (enviro == World.Environment.THE_END) {
           player.playEffect(player.getLocation(), Effect.BOW_FIRE, 0);
           player.teleport(norm.getSpawnLocation());
        } else if (enviro == World.Environment.NETHER) {
           player.playEffect(player.getLocation(), Effect.EXTINGUISH, 0);
            player.sendMessage(ChatColor.GOLD + player.getDisplayName() + ChatColor.GOLD +
                    ", don't expect to be able to skip the normal world you failure. " +
                    "Also, you're adopted. You were abandoned at birth. You're fat too.");
        } else {
            player.playEffect(player.getLocation(), Effect.EXTINGUISH, 0);
            player.sendMessage(ChatColor.RED + "Where the hell are you? I do not know of your current world.");
        }
    }

    public void directToggle(String name, Player player) {
        World world = player.getWorld();
        World def = Bukkit.getServer().getWorlds().get(0);
        Location loc = player.getLocation();
        if (world.getName().equalsIgnoreCase(def.getName())) {
            loc.setWorld(Bukkit.getServer().getWorld(name));
            loc.setY(world.getHighestBlockYAt(loc));
            player.teleport(loc);
        } else if (world.getEnvironment() == World.Environment.THE_END) {
            loc.setWorld(def);
            loc.setY(def.getHighestBlockYAt(loc));
            player.setFallDistance(0);
            player.teleport(loc);
        } else {
            player.sendMessage(ChatColor.RED + "Unknown world!");
        }
    }

    public class SkylandsCommands extends BaseCommand {
        @Command(aliases = {"skyjump"},
                usage = "/<command>", desc = "Switch manually between worlds",
                flags = "", min = 0, max = 0)
        @CommandPermissions({"narwhal.skylands.tp"})
        public void skyjump(CommandContext args,
                           CommandSender sender) throws CommandException {

            Player player = checkPlayer(sender);
            skyToggle(getSkyWorldName(), player);

        }
    }
}

package com.zachsthings.narwhal.components;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.zachsthings.narwhal.Narwhal;
import com.zachsthings.narwhal.commands.BaseCommand;
import com.zachsthings.narwhal.events.PlayerAuthenticateEvent;
import com.zachsthings.narwhal.events.PlayerHandshakeEvent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.EventHandler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zml2008
 */
public class AuthenticationComponent extends AbstractComponent implements Listener {

    private Map<String, String> authedUsers = new HashMap<String, String>();
    private int unauthNumber = 0;
    private boolean mcAuthOffline = false;

    @Override
    public void postInitialize() {
        Narwhal.server().getPluginManager().registerEvents(this, Narwhal.inst());
        Narwhal.inst().registerCommands(AuthenticationCommands.class, this);
        Bukkit.getServer().getScheduler().scheduleAsyncRepeatingTask(Narwhal.inst(), new Runnable() {
            public void run() {
                HttpURLConnection conn = null;
                try {
                    conn = (HttpURLConnection) (new URL("https://login.minecraft.net/session").openConnection());
                    conn.setConnectTimeout(4500);
                    conn.connect();
                    if ((conn.getResponseCode() != 400)) {
                        mcAuthOffline = true;
                    } else {
                        mcAuthOffline = false;
                    }
                } catch (MalformedURLException e) {
                    mcAuthOffline = true;
                } catch (IOException e) {
                    mcAuthOffline = true;
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }

            }
        }, 0, 5 * 60 * 20);
    }

    public void setLoginName(String name, InetAddress addr) {
        authedUsers.put(addr.getHostAddress(), name);
    }

    public void clearLoginName(InetAddress addr) {
        if (!authedUsers.containsKey(addr.getHostAddress()))
        authedUsers.remove(addr.getHostAddress());
    }

    public String getUnauthPrefix() {
        return getConfiguration().getString("unauth-prefix", "UnauthGuest");
    }

    public String restoreName(String orig, final InetAddress addr) {
        boolean auth = authedUsers.containsKey(addr.getHostAddress());
        String name = auth ? authedUsers.get(addr.getHostAddress()) : orig;
        StringBuilder sb = new StringBuilder(name);
        while (Bukkit.getServer().getPlayerExact(sb.toString()) != null) {
            sb.append("_");
        }
        name = sb.toString();
        if (name != null && !name.equals(orig)) {
            if (auth)
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Narwhal.inst(), new Runnable() {
                    public void run() {
                        clearLoginName(addr);
                    }
                }, 36000L);
            return name;
        }
        return name;
    }

    @EventHandler
    public void onPlayerAuthenticate(PlayerAuthenticateEvent event) {
        if (!event.getSessionOfflineMode()) {
            if (authedUsers.containsKey(event.getAddress().getHostAddress())) {
                event.setName(restoreName(event.getName(), event.getAddress()));
            } else {
                event.setName(getUnauthPrefix() + ++unauthNumber);
            }
        } else {

        }
    }

    @EventHandler
    public void handleQuit(PlayerQuitEvent event) {
        if (event.getPlayer().getName().startsWith(getUnauthPrefix())) {
            if (--unauthNumber < 0) {
                unauthNumber = 0;
            }
        }
    }

    @EventHandler
    public void onPlayerHandshake(PlayerHandshakeEvent event) {
        String name = event.getName();
        if (mcAuthOffline || name.equalsIgnoreCase("Player") || name.equalsIgnoreCase("Pirate")) {
            event.setCheckAuth(false);
        }
    }

    private boolean canLogin(CommandSender sender, String user, String pass) {
        if (pass == null) {
            if (Narwhal.inst().hasPermission(sender, "narwhal.login.all"))
                return true;
        } else {
            byte[] bytesOfMessage = pass.getBytes();

            MessageDigest md;
            try {
                md = MessageDigest.getInstance("SHA-512");
            } catch (NoSuchAlgorithmException e) {
                return false;
            }
            byte[] theDigest = md.digest(bytesOfMessage);
            String passHash = Arrays.toString(theDigest);
            // TODO setup database
            return true;
        }
        return true;
    }

    private class AuthenticationCommands extends BaseCommand {
        @Command(aliases = {"login", "fullfake"},
                usage = "<user> [password]",
                desc = "Login with a fully different username",
                min = 1, max = 2)
        @CommandPermissions({"narwhal.login"})
        public void login(CommandContext args, CommandSender sender) throws CommandException {
            Player player = checkPlayer(sender);
            String pass = args.argsLength() > 1 ? args.getString(1) : null;
            if (canLogin(sender, args.getString(0), pass)) {
                setLoginName(args.getString(0), player.getAddress().getAddress());
                player.kickPlayer("Please relog to use your new name.");
            } else {
                throw new CommandException("Wrong credentials entered");
            }
        }
    }
}

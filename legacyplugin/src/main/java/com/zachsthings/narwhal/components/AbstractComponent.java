package com.zachsthings.narwhal.components;

import com.sk89q.util.yaml.YAMLNode;

/**
 * @author zml2008
 */
public abstract class AbstractComponent {
    private YAMLNode config;

    public void init(YAMLNode config) {
        this.config = config;
    }

    public YAMLNode getConfiguration() {
        return config;
    }

    public abstract void postInitialize();

    public void unload() {}
}

package com.zachsthings.narwhal.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zml2008
 */
public class Munge {
    private static Map<String, String> characterReplacements = new HashMap<String, String>(52);

    static {
        characterReplacements.put("a", "ä");
        // 'b': 'Б',
        characterReplacements.put("c", "ċ");
        characterReplacements.put("d", "đ");
        characterReplacements.put("e", "ë");
        characterReplacements.put("f", "ƒ");
        characterReplacements.put("g", "ġ");
        characterReplacements.put("h", "ħ");
        characterReplacements.put("i", "í");
        characterReplacements.put("j", "ĵ");
        characterReplacements.put("k", "ķ");
        characterReplacements.put("l", "ĺ");
        // 'm', 'ṁ',
        characterReplacements.put("n", "ñ");
        characterReplacements.put("o", "ö");
        characterReplacements.put("p", "ρ");
        // 'q', 'ʠ',
        characterReplacements.put("r", "ŗ");
        characterReplacements.put("s", "š");
        characterReplacements.put("t", "ţ");
        characterReplacements.put("u", "ü");
        // 'v', '',
        characterReplacements.put("w", "ω");
        characterReplacements.put("x", "χ");
        characterReplacements.put("y", "ÿ");
        characterReplacements.put("z", "ź");
        characterReplacements.put("A", "Å");
        characterReplacements.put("B", "Β");
        characterReplacements.put("C", "Ç");
        characterReplacements.put("D", "Ď");
        characterReplacements.put("E", "Ē");
        // ('F', 'Ḟ',
        characterReplacements.put("G", "Ġ");
        characterReplacements.put("H", "Ħ");
        characterReplacements.put("I", "Í");
        characterReplacements.put("J", "Ĵ");
        characterReplacements.put("K", "Ķ");
        characterReplacements.put("L", "Ĺ");
        characterReplacements.put("M", "Μ");
        characterReplacements.put("N", "Ν");
        characterReplacements.put("O", "Ö");
        characterReplacements.put("P", "Р");
        // 'Q', 'Ｑ',
        characterReplacements.put("R", "Ŗ");
        characterReplacements.put("S", "Š");
        characterReplacements.put("T", "Ţ");
        characterReplacements.put("U", "Ů");
        // 'V', 'Ṿ',
        characterReplacements.put("W", "Ŵ");
        characterReplacements.put("X", "Χ");
        characterReplacements.put("Y", "Ỳ");
        characterReplacements.put("Z", "Ż");
    }

    public static String munge(String in, int mungeCount) {
        return "_" + in.substring(1);
        /* int reps = 0;
        for (int i = 0; i < in.length(); i++) {
            System.out.println("current: " + in.substring(i, i + 1));
            if (characterReplacements.containsKey(in.substring(i, i + 1))) {
                in = in.substring(0, i) + characterReplacements.get(in.substring(i, i + 1)) + in.substring(i + 1, in.length());
                if (++reps >= mungeCount) break;
            }
        }
        return in; */
    }
}

package com.zachsthings.narwhal;

/**
 * @author zml2008
 */
public class NarwhalRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 6783746608830942166L;

    public NarwhalRuntimeException(String message) {
        super(message);
    }
}

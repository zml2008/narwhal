CREATE TABLE IF NOT EXISTS entities (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `type` tinyint(1) NOT NULL,
    `name` varchar(500) NOT NULL,
    PRIMARY KEY(`id`)
);

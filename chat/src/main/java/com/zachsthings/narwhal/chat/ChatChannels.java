package com.zachsthings.narwhal.chat;

import com.sk89q.commandbook.CommandBook;
import com.sk89q.commandbook.session.PersistentSession;
import com.zachsthings.libcomponents.ComponentInformation;
import com.zachsthings.libcomponents.bukkit.BukkitComponent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Chat channels, designed mostly for GoMySQL
 */
@ComponentInformation(friendlyName = "Chat Channels", desc = "Chat channels for servers")
public class ChatChannels extends BukkitComponent implements Listener {

    @Override
    public void enable() {
        CommandBook.registerEvents(this);
        registerCommands(Commands.class);
    }

    @EventHandler
    public void onPlayerChat(PlayerChatEvent event) {

    }

    private static class ChatChannel {
        public final Set<String> members = new HashSet<String>();
        public final String prefixFormat = "[%s]";
    }

    private static class ChatSession extends PersistentSession {
        public static final long MAX_TIME = TimeUnit.MINUTES.toMillis(30);
        public ChatSession() {
            super(MAX_TIME);
        }
    }

    public class Commands {

    }
}

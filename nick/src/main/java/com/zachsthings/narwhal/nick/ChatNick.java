package com.zachsthings.narwhal.nick;

import com.sk89q.commandbook.CommandBook;
import com.sk89q.commandbook.util.PlayerUtil;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.zachsthings.libcomponents.ComponentInformation;
import com.zachsthings.libcomponents.bukkit.BukkitComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@ComponentInformation(friendlyName = "Chat Nick", desc = "Gives a player's name a random color and allows setting player nicks")
public class ChatNick extends BukkitComponent implements Listener {
    private static final Random rand = new Random();
    private static final List<ChatColor> acceptableColors = new ArrayList<ChatColor>();
    static {
        for (ChatColor color : ChatColor.values()) {
            if (color.isColor() && color != ChatColor.BLACK) {
                acceptableColors.add(color);
            }
        }
    }

    public void colorDispName(String nick, Player player) {
        ChatColor color = acceptableColors.get(rand.nextInt(acceptableColors.size() - 1));
        player.setDisplayName(color + nick + ChatColor.RESET);
    }

    @EventHandler
    public void onPlayerLogin(PlayerJoinEvent event) {
        colorDispName(event.getPlayer().getDisplayName(), event.getPlayer());
    }

    @Override
    public void enable() {
        CommandBook.registerEvents(this);
        registerCommands(NickCommands.class);
        for (Player player : CommandBook.server().getOnlinePlayers()) {
            colorDispName(player.getDisplayName(), player);
        }
    }

    public class NickCommands {
        @Command(aliases = {"nick"},
                usage = "[name]", desc = "Get a player's nickname",
                flags = "", min = 0, max = 1)
        @CommandPermissions({"narwhal.nick"})
        public void nick(CommandContext args,
                         CommandSender sender) throws CommandException {
            Player player = PlayerUtil.checkPlayer(sender);
            if (args.argsLength() == 1) {
                Player fake = PlayerUtil.matchSinglePlayer(sender, args.getString(0));
                String fakedn = null;
                if (fake != null) {
                    fakedn = fake.getDisplayName();
                } else {
                    /*NickPersist fakepersist = Narwhal.inst().getDatabase().find(NickPersist.class).where().ieq("realName", args.getString(0,player.getName())).findUnique();
                    if (fakepersist != null && !fakepersist.getFakeName().equals("")) {
                        fakedn = fakepersist.getFakeName();
                    }*/
                }
                if (fakedn != null) {
                    sender.sendMessage(ChatColor.BLUE + args.getString(0) + "\'s nick is " + fakedn + ".");
                } else {
                    sender.sendMessage(ChatColor.RED + "Player\'s nick not found");
                }
            } else {
                sender.sendMessage(ChatColor.BLUE + "Your nick is " + player.getDisplayName() + ChatColor.BLUE + ".");
            }
        }
        @Command(aliases = {"setnick"},
                usage = "-r [name] <nick>", desc = "Set a player's nickname",
                flags = "r", min = 1, max = 2)
        @CommandPermissions({"narwhal.nick.set"})
        public void setnick(CommandContext args,
                            CommandSender sender) throws CommandException {
            /*if (args.hasFlag('r')) {
                NickPersist realPersist = Narwhal.inst().getDatabase().find(NickPersist.class).where().ieq("realName", args.getString(0)).findUnique();
                Player real = Bukkit.getServer().getPlayer(args.getString(0));
                if (real !=null && realPersist !=null) {
                    colorDispName(real.getName(), real);
                    realPersist.setFakeName(real.getName());
                    Narwhal.inst().getDatabase().save(realPersist);
                }

            } else*/ {
                if (args.argsLength() == 1) {
                    Player player = PlayerUtil.checkPlayer(sender);
                    /*NickPersist setPersist = Narwhal.inst().getDatabase().find(NickPersist.class).where().ieq("realName",  player.getName()).findUnique();
                    if (setPersist == null) {
                        setPersist = new NickPersist();
                        setPersist.setRealName(player.getName());
                    }
                    setPersist.setFakeName(args.getString(0));
                    Narwhal.inst().getDatabase().save(setPersist);*/
                    colorDispName(args.getString(0), player);
                    player.sendMessage(ChatColor.BLUE + "Your nick set to " + args.getString(0) + ".");

                } else if (args.argsLength() == 2) {
                    Player player = PlayerUtil.checkPlayer(sender);
                    Player real = PlayerUtil.matchSinglePlayer(sender, args.getString(0));
                    if (real != null) {
                        /*NickPersist realPersist = Narwhal.inst().getDatabase().find(NickPersist.class).where().ieq("realName", args.getString(0)).findUnique();
                        if (realPersist == null) {
                            realPersist = new NickPersist();
                            realPersist.setRealName(real.getName());
                        }
                        realPersist.setFakeName(args.getString(1));*/
                        colorDispName(args.getString(1), real);
                        //Narwhal.inst().getDatabase().save(realPersist);
                        if (player != real) {
                            sender.sendMessage(ChatColor.BLUE + args.getString(0) + "\'s nick set to " + args.getString(1) + ".");
                        }
                        real.sendRawMessage(ChatColor.BLUE + "Your nick set to " + args.getString(1) + ".");
                    }

                }
            }
        }
    }
}

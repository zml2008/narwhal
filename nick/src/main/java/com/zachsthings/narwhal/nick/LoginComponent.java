package com.zachsthings.narwhal.nick;

import com.sk89q.commandbook.CommandBook;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.zachsthings.libcomponents.ComponentInformation;
import com.zachsthings.libcomponents.bukkit.BukkitComponent;
import com.zachsthings.libcomponents.config.ConfigurationBase;
import com.zachsthings.libcomponents.config.Setting;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;

import java.util.HashSet;
import java.util.Set;

/**
 * @author zml2008
 */
@ComponentInformation(friendlyName = "Login", desc = "Provides a customised login/quit/kick message, whitelist, and maintenance mode")
public class LoginComponent extends BukkitComponent implements Listener {
    private final Set<String> playersKicked = new HashSet<String>();
    private String tempMotd;
    private LocalConfiguration config;

    @Override
    public void enable() {
        CommandBook.registerEvents(this);
        registerCommands(LoginManagementCommands.class);
        config = configure(new LocalConfiguration());
    }

    @Override
    public void reload() {
        super.reload();
        configure(config);
    }

    private static class LocalConfiguration extends ConfigurationBase {
        @Setting("whitelist.denied-message") public String whitelistDeniedMessage = "You are not whitelisted!";
        @Setting("whitelist.enabled") public boolean whitelistEnabled = true;
        @Setting("maintenance-mode.message") public String maintenanceModeMessage = "This server is in maintenance mode!";
        @Setting("maintenance-mode.enabled") public boolean maintenanceModeEnabled;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (playersKicked.contains(event.getPlayer().getName())) {
            event.setQuitMessage(ChatColor.BLUE + event.getPlayer().getDisplayName() +
                    ChatColor.BLUE + " is shoved out of the TARDIS, in front of an army of Daleks!");
        } else {
            event.setQuitMessage(ChatColor.BLUE + "The TARDIS vanishes with " +
                    event.getPlayer().getDisplayName() + ChatColor.BLUE + "!");
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        playersKicked.remove(event.getPlayer().getName());
        event.setJoinMessage(ChatColor.BLUE + "The TARDIS has landed... " +
                event.getPlayer().getDisplayName() + ChatColor.BLUE + " has joined the game.");
    }

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event) {
        playersKicked.add(event.getPlayer().getName());
    }

    @EventHandler
    public void serverListPing(ServerListPingEvent event) {
        if (config.maintenanceModeEnabled) {
            event.setMotd(config.maintenanceModeMessage);
        } else if (tempMotd != null) {
            event.setMotd(tempMotd);
        }
    }

    @EventHandler
    public void playerLogin(PlayerLoginEvent event) {
        if (config.maintenanceModeEnabled && !CommandBook.inst().hasPermission(event.getPlayer(), "narwhal.login.overrideoffline")) {
            event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, config.maintenanceModeMessage);
        }
        if (config.whitelistEnabled && !CommandBook.inst().hasPermission(event.getPlayer(), "narwhal.login.whitelisted")) {
            event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, config.whitelistDeniedMessage);
        }
    }



    public class LoginManagementCommands {
        @Command(aliases = {"setmotd"},
                usage = "[-t ticks] <motd>", desc = "Set the server's MOTD to a string for some length of time",
                flags = "t:", min = 1, max = 1)
        @CommandPermissions("narwhal.login.setmotd")
        public void setMotd(CommandContext args, CommandSender sender) throws CommandException {
            tempMotd = args.getJoinedStrings(0);
            if (args.hasFlag('t')) {
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(CommandBook.inst(), new Runnable() {
                    public void run() {
                        tempMotd = null;
                    }
                }, args.getFlagInteger('t'));
            }
            sender.sendMessage(ChatColor.BLUE + "Server's MOTD set to " + args.getString(0));
        }

        @Command(aliases = {"maintenance"},
                usage = "<enable|disable|status>", desc = "Control the server's maintenance mode status",
                min = 1, max = 1)
        public void maintenance(CommandContext args, CommandSender sender) throws CommandException {
            String action = args.getString(0);
            CommandBook.inst().checkPermission(sender, "narwhal.login.maintenance." + action);
            if (action.equalsIgnoreCase("enable")) {
                if (config.maintenanceModeEnabled) {
                    throw new CommandException("Maintenance mode already enabled!");
                }
                config.maintenanceModeEnabled = true;
                saveConfig(config);
                sender.sendMessage(ChatColor.BLUE + "Maintenance mode enabled!");
            } else if (action.equalsIgnoreCase("disable")) {
                if (!config.maintenanceModeEnabled) {
                    throw new CommandException("Maintenance mode already disabled");
                }
                config.maintenanceModeEnabled = false;
                saveConfig(config);
                sender.sendMessage(ChatColor.BLUE + "Maintenance mode disabled");
            } else if (action.equalsIgnoreCase("status")) {
                sender.sendMessage(ChatColor.BLUE + "Maintenance mode is: " + (config.maintenanceModeEnabled ? "enabled" : "disabled"));
                sender.sendMessage(ChatColor.AQUA + "The maintenance mode message is: " + config.maintenanceModeMessage);
            } else {
                throw new CommandException("Unknown action for command!");
            }
        }

        @Command(aliases = {"whitelist"},
                usage = "<enable|disable|check <name>>", desc = "Manage the server's whitelist",
        min = 1, max = 2)
        @CommandPermissions("narwhal.login.whitelist")
        public void whitelist(CommandContext args, CommandSender sender) throws CommandException {
            String action = args.getString(0);
            if (action.equalsIgnoreCase("enable")) {
                if (config.whitelistEnabled) {
                    throw new CommandException("Whitelist already enabled!");
                }
                config.whitelistEnabled = true;
                saveConfig(config);
                sender.sendMessage(ChatColor.BLUE + "Whitelist enabled!");
            } else if (action.equalsIgnoreCase("disable")) {
                if (!config.whitelistEnabled) {
                    throw new CommandException("Whitelist already disabled");
                }
                config.whitelistEnabled = false;
                saveConfig(config);
                sender.sendMessage(ChatColor.BLUE + "Whitelist disabled");
            } else if (action.equalsIgnoreCase("check")) {
                if (args.argsLength() < 2) throw new CommandException("No player specified");
            sender.sendMessage(ChatColor.BLUE + "Player " + args.getString(1) + " " +
                    (CommandBook.inst().getPermissionsResolver().
                            hasPermission(args.getString(1), "narwhal.login.whitelisted")
                            ? "is" : "is not")+ " whitelisted");
            } else {
                sender.sendMessage(ChatColor.BLUE + "Whitelist: " + (config.whitelistEnabled ? "enabled" : "disabled"));
                sender.sendMessage(ChatColor.AQUA + "The whitelist message is: " + config.whitelistDeniedMessage);
            }
        }
    }
}

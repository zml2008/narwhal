package com.zachshings.narwhal.timedcommands;

import com.sk89q.commandbook.CommandBook;
import com.zachsthings.libcomponents.ComponentInformation;
import com.zachsthings.libcomponents.bukkit.BukkitComponent;
import com.zachsthings.libcomponents.config.ConfigurationBase;
import com.zachsthings.libcomponents.config.Setting;
import org.apache.commons.lang.math.NumberUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author zml2008
 */
@ComponentInformation(friendlyName = "TimedCommands", desc = "Allows the execution of several commands at repeating intervals")
public class TimedCommands extends BukkitComponent implements Runnable {
    private final ScheduledExecutorService scheduledService = Executors.newSingleThreadScheduledExecutor();
    private static final String NAME_KEY = "name", INTERVAL_KEY = "interval";
    private final Map<String, Integer> activeIntervals = new HashMap<String, Integer>();
    private LocalConfiguration config;

    @Override
    public void enable() {
        loadConfig();
        scheduledService.scheduleAtFixedRate(this, 1, 1, TimeUnit.SECONDS);
    }

    @Override
    public void reload() {
        super.reload();
        loadConfig();
        activeIntervals.clear();
    }

    @Override
    public void disable() {
        super.disable();
        scheduledService.shutdown();
    }

    private void loadConfig() {
        config = configure(new LocalConfiguration());
        for (Iterator<Map<String, String>> i = config.commands.iterator(); i.hasNext();) {
            Map<String, String> command = i.next();
            if (!command.containsKey(NAME_KEY)) {
                CommandBook.logger().warning("Command entry in TimedCommands configuration does not have necessary name entry!");
                i.remove();
                continue;
            }

            if (!command.containsKey(INTERVAL_KEY)) {
                CommandBook.logger().warning("Command entry for '"  + command.get(NAME_KEY) + "' does not contain required '" + INTERVAL_KEY + "' entry!");
                i.remove();
            }
        }
    }

    private static class LocalConfiguration extends ConfigurationBase {
        @Setting("commands") public List<Map<String, String>> commands = createCommandDefaults();

       private static List<Map<String, String>> createCommandDefaults() {
           Map<String, String> command = new HashMap<String, String>();
           command.put(NAME_KEY, "pong");
           command.put(INTERVAL_KEY, "60");
           return new ArrayList<Map<String, String>>(Collections.singletonList(command));
       }
    }

    public void run() {
        for (Map<String, String> command : config.commands) {
            final String commandName = command.get(NAME_KEY);
            Integer remainingTime = activeIntervals.get(commandName);
            if (remainingTime == null) {
                remainingTime = NumberUtils.toInt(command.get(INTERVAL_KEY));
            }
            if (--remainingTime <= 0) {
                CommandBook.server().getScheduler().callSyncMethod(CommandBook.inst(), new Callable<Boolean>() {
                    public Boolean call() throws Exception {
                        return CommandBook.server().dispatchCommand(CommandBook.server().getConsoleSender(), commandName);
                    }
                });
                activeIntervals.remove(commandName);
            } else {
                activeIntervals.put(commandName, remainingTime);
            }
        }
    }
}
